//
//  Constants.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/8/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

// The uniform place where we state all the storyboard we have in our application
enum Storyboard: String {
    case loading = "Loading"
    case logIn = "LogIn"
    case register = "Register"
    case main = "Main"
    case event = "Message"
    case setting = "Setting"
    case conversation = "Conversation"
}
