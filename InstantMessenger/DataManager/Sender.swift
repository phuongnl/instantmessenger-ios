//
//  Sender.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/10/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import UIKit

struct Sender {
    var senderId: String
    var displayName: String
}

extension Sender: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case senderId = "sender_id"
        case displayName = "display_name"
    }
    
}
