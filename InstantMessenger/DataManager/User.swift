//
//  User.swift
//  SmileMirror
//
//  Created by Lucas Lee on 8/6/18.
//  Copyright © 2018 OMM. All rights reserved.
//

import UIKit

struct User {
    var userName: String
    var email: String?
    var userId: Int
}

extension User: Codable {
    
    private enum CodingKeys: String, CodingKey {
        case userName = "user_name"
        case email
        case userId = "user_id"
    }
    
}
