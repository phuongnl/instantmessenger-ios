//
//  AccessToken.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/8/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import UIKit

struct AccessToken {
    var accessToken: String
    var expiresIn: TimeInterval
    var tokenType: String
    var scope: String?
}

extension AccessToken: Codable {
    
    private enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case expiresIn = "expires_in"
        case tokenType = "token_type"
        case scope
    }
    
}
