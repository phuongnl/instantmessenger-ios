//
//  UserLocalDataSource.swift
//  SmileMirror
//
//  Created by Lucas Lee on 8/6/18.
//  Copyright © 2018 OMM. All rights reserved.
//

import UIKit

class UserLocalDataSource: NSObject {
    
    private let kTokenCache = "token_cache"
    
    func getToken() -> AccessToken? {
        let defaults = UserDefaults.standard
        guard let tokenData = defaults.object(forKey: kTokenCache) as? Data,
            let accessToken = try? PropertyListDecoder().decode(AccessToken.self, from: tokenData) else {
                return nil
        }
        return accessToken
    }
    
    func cache(token: AccessToken) {
        let defaults = UserDefaults.standard
        defaults.set(try? PropertyListEncoder().encode(token), forKey: kTokenCache)
        defaults.synchronize()
    }
    
    func clear() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: kTokenCache)
        defaults.synchronize()
    }
    
}
