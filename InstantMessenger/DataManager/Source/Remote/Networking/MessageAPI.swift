//
//  MessageAPI.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/10/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import Alamofire

enum MessageAPI: URLRequestConvertible {
    
    case get(conversationId: String)
    
    var method: HTTPMethod {
        switch self {
        case .get:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .get(let conversationId):
            return "/messages/\(conversationId)"
        }
    }
    
    var headers: [String: String]? {
        var headers = [String: String]()
        if let accessToken = UserRepository().getToken()?.accessToken {
            headers[JSONKey.authorization.rawValue] = JSONKey.bearer.rawValue + " " + accessToken
        }
        return headers
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try APIEndpoint.baseURL.rawValue.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        if let headers = headers {
            for (key, value) in headers {
                urlRequest.addValue(value, forHTTPHeaderField: key)
            }
        }
        
        switch self {
        case .get:
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
        }
        
        return urlRequest
    }
    
}
