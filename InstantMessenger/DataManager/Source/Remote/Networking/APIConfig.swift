//
//  APIConfig.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/10/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import Alamofire

enum APIEndpoint: String {
    case baseURL            = "https://locaslhost:8080/api/v1"
    case socket             = "ws://localhost:8080/"
}

extension Request {
    
    public func debugLog() -> Self {
        #if DEBUG
        debugPrint(self)
        #endif
        return self
    }
    
}
