//
//  JSONKey.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/10/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import Foundation

enum JSONKey: String {
    
    case authorization      = "Authorization"
    case contentType        = "Content-type"
    case accessToken        = "access_token"
    case expiresIn          = "expires_in"
    case tokenType          = "token_type"
    case bearer             = "Bearer"
    case email              = "email"
    case password           = "password"
    case userId             = "user_id"
    
}
