//
//  UserAPI.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/10/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import Alamofire

enum UserAPI: URLRequestConvertible {
    
    case logIn(email: String, password: String)
    case signUp(email: String, password: String)
    case info(userId: String)
    
    var method: HTTPMethod {
        switch self {
        case .logIn, .signUp:
            return .post
        case .info:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .logIn:
            return "/login"
        case .signUp:
            return "/signup"
        case .info(let userId):
            return "/user/\(userId)"
        }
    }
    
    var headers: [String: String]? {
        switch self {
        case .logIn, .signUp:
            return nil
        case .info:
            var headers = [String: String]()
            if let accessToken = UserRepository().getToken()?.accessToken {
                headers[JSONKey.authorization.rawValue] = JSONKey.bearer.rawValue + " " + accessToken
            }
            return headers
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try APIEndpoint.baseURL.rawValue.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        if let headers = headers {
            for (key, value) in headers {
                urlRequest.addValue(value, forHTTPHeaderField: key)
            }
        }
        
        switch self {
        case .logIn(let email, let password):
            var params = Parameters()
            params[JSONKey.email.rawValue] = email
            params[JSONKey.password.rawValue] = password
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: params)
        case .signUp(let email, let password):
            var params = Parameters()
            params[JSONKey.email.rawValue] = email
            params[JSONKey.password.rawValue] = password
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: params)
        case .info:
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
        }
        
        return urlRequest
    }
    
}
