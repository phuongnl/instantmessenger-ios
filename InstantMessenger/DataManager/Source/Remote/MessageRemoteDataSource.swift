//
//  MessageRemoteDataSource.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/8/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import UIKit
import Starscream

protocol MessageRemoteDataSourceDelegate: class {
    func didReceiveMessage()
    func didReceiveData()
}

class MessageRemoteDataSource: NSObject {
    
    private var socket: WebSocket!
    private var messages: [String]!
    private var data: [Data]!
    weak var delegate: MessageRemoteDataSourceDelegate?
    
    override init() {
        super.init()
        self.socket = WebSocket(url: URL(string: APIEndpoint.socket.rawValue)!)
        self.socket.delegate = self
        self.socket.connect()
        self.messages = [String]()
        self.data = [Data]()
    }
    
    func send(message: String, completion: @escaping (() -> Void)) {
        self.socket.write(string: message, completion: completion)
    }
    
    func send(data: Data, completion: @escaping (() -> Void)) {
        self.socket.write(data: data, completion: completion)
    }
    
    func messageFromQueue() -> String? {
        if self.messages.count > 0 {
            return self.messages[0]
        }
        return nil
    }
    
    func dataFromQueue() -> Data? {
        if self.data.count > 0 {
            return self.data[0]
        }
        return nil
    }
    
}

extension MessageRemoteDataSource: WebSocketDelegate {
    
    func websocketDidConnect(socket: WebSocketClient) {
        log.verbose("websocketDidConnect")
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        log.verbose("websocketDidDisconnect \(error ?? NSError())")
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        self.messages.append(text)
        self.delegate?.didReceiveMessage()
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        self.data.append(data)
        self.delegate?.didReceiveData()
    }
    
}
