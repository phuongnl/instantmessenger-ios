//
//  MessageRepository.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/8/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import UIKit
import MessageKit

class MessageRepository: NSObject {
    
    var messageLocalDataSource = MessageLocalDataSource()
    var messageRemoteDataSource = MessageRemoteDataSource()
    
    var sender: MKSender!
    
}
