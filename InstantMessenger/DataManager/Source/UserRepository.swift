//
//  UserRepository.swift
//  SmileMirror
//
//  Created by Lucas Lee on 8/6/18.
//  Copyright © 2018 OMM. All rights reserved.
//

import UIKit

class UserRepository: NSObject {
    
    let userRemoteDataSource = UserRemoteDataSource()
    let userLocalDataSource = UserLocalDataSource()
    
    func login() {
        
    }
    
    func getToken() -> AccessToken? {
        return self.userLocalDataSource.getToken()
    }

    
}
