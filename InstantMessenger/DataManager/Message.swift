//
//  Message.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/8/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import UIKit

struct Message {
    var sender: Sender
    var messageId: String
    var sendDate: Date
    var kind: String
    var content: String
}

extension Message: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case sender
        case messageId = "message_id"
        case sendDate = "send_date"
        case kind
        case content
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.sender = try container.decode(Sender.self, forKey: .sender)
        self.messageId = try container.decode(String.self, forKey: .messageId)
        self.sendDate = try container.decode(Date.self, forKey: .sendDate)
        self.kind = try container.decode(String.self, forKey: .kind)
        self.content = try container.decode(String.self, forKey: .content)
    }
    
}
