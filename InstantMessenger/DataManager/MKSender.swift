//
//  MKSender.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/10/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import Foundation
import MessageKit

struct MKSender: SenderType, Equatable {
    
    var senderId: String
    var displayName: String
    
}
