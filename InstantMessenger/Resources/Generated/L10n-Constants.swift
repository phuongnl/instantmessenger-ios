// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable function_parameter_count identifier_name line_length type_body_length
internal enum L10n {
  /// Cancel
  internal static let cancel = L10n.tr("Localizable", "Cancel")
  /// Conversation
  internal static let conversation = L10n.tr("Localizable", "Conversation")
  /// Log In
  internal static let logIn = L10n.tr("Localizable", "LogIn")
  /// Message
  internal static let message = L10n.tr("Localizable", "Message")
  /// OK
  internal static let ok = L10n.tr("Localizable", "OK")
  /// Register
  internal static let register = L10n.tr("Localizable", "Register")
  /// Setting
  internal static let setting = L10n.tr("Localizable", "Setting")
}
// swiftlint:enable function_parameter_count identifier_name line_length type_body_length

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
