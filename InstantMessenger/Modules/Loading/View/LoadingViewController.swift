//
//  LoadingViewController.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/8/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import UIKit

protocol LoadingViewProtocol: class {
    func showLogInView()
    func showMainView()
}

class LoadingViewController: UIViewController {

    var presenter: LoadingPresenterProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.presenter.checkLoginState()
    }

}

extension LoadingViewController: LoadingViewProtocol {
    
    func showLogInView() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let navLogInViewController: UINavigationController = UIStoryboard(.logIn).instantiateViewController(withIdentifier: "NavLogInViewController")
        guard let logInViewController = navLogInViewController.viewControllers.first as? LogInViewController else { return }
        let logInPresenter = LogInPresenter()
        logInViewController.presenter = logInPresenter
        logInPresenter.view = logInViewController
        appDelegate.window?.rootViewController = navLogInViewController
    }
    
    func showMainView() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let mainViewController: MainViewController = UIStoryboard(.main).instantiateViewController(withIdentifier: "MainViewController")
        let mainPresenter = MainPresenter()
        mainPresenter.view = mainViewController
        mainViewController.presenter = mainPresenter
        appDelegate.window?.rootViewController = mainViewController
    }
    
}
