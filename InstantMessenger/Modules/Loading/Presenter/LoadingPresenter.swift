//
//  LoadingPresenter.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/8/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import Foundation

protocol LoadingPresenterProtocol: class {
    func checkLoginState()
}

class LoadingPresenter: LoadingPresenterProtocol {

    var view: LoadingViewProtocol!
    var userRepository = UserRepository()
    
    func checkLoginState() {
        if self.userRepository.getToken() == nil {
            self.view.showLogInView()
        } else {
            self.view.showMainView()
        }
    }
    
}
