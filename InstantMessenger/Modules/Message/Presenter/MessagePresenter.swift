//
//  MessagePresenter.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/8/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import Foundation

protocol MessagePresenterProtocol: class {
    func handleDisSelectRow(_ message: Message)
}

class MessagePresenter: MessagePresenterProtocol {
    
    var view: MessageViewProtocol!
    
    func handleDisSelectRow(_ message: Message) {
        self.view.showConversation(conversationId: message.messageId)
    }
    
}
