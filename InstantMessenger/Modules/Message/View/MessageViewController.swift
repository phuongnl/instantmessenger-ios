//
//  MessageViewController.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/8/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import UIKit

protocol MessageViewProtocol: class {
    func showConversation(conversationId: String)
}

class MessageViewController: UIViewController, MessageViewProtocol {

    @IBOutlet weak var tableView: UITableView!

    var presenter: MessagePresenterProtocol!
    var messages = [Message]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = L10n.message
        self.tabBarItem.image = #imageLiteral(resourceName: "bubble2-75")
        let sender = Sender(senderId: "1", displayName: "1")
        self.messages = [
            Message(sender: sender, messageId: "1", sendDate: Date(), kind: "1", content: "1213"),
            Message(sender: sender, messageId: "1", sendDate: Date(), kind: "1", content: "1213"),
            Message(sender: sender, messageId: "1", sendDate: Date(), kind: "1", content: "1213"),
            Message(sender: sender, messageId: "1", sendDate: Date(), kind: "1", content: "1213"),
            Message(sender: sender, messageId: "1", sendDate: Date(), kind: "1", content: "1213")
        ]
        self.tableView.register(MessageCell.self)
        self.tableView.tableFooterView = UIView()
    }

    func showConversation(conversationId: String) {
        let conversationViewController: ConversationViewController = UIStoryboard(.conversation).instantiateViewController()
        let conversationPresenter = ConversationPresenter()
        conversationViewController.presenter = conversationPresenter
        conversationPresenter.view = conversationViewController
        self.navigationController?.pushViewController(conversationViewController, animated: true)
    }
    
}

extension MessageViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MessageCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        
        return cell
    }
    
}

extension MessageViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter.handleDisSelectRow(self.messages[indexPath.row])
    }
    
}
