//
//  SettingViewController.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/8/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import UIKit

protocol SettingViewProtocol: class {
    
}

class SettingViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var presenter: SettingPresenter!
    var numberOfItem = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = L10n.setting
        self.tabBarItem.image = #imageLiteral(resourceName: "setting-gear-cog-75")
        self.tableView.register(SettingCell.self)
        self.tableView.tableFooterView = UIView()
    }

}

extension SettingViewController: SettingViewProtocol {
    
}

extension SettingViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SettingCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        
        cell.selectionStyle = .none
        
        return cell
    }
    
}

extension SettingViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
