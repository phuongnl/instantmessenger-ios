//
//  SettingPresenter.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/8/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import Foundation

protocol SettingPresenterProtocol: class {
    
}

class SettingPresenter: SettingPresenterProtocol {
    
    var view: SettingViewProtocol!
    
}
