//
//  MainPresenter.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/8/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import Foundation

protocol MainPresenterProtocol: class {

}

class MainPresenter: MainPresenterProtocol {
    
    var view: MainViewProtocol!
    
}
