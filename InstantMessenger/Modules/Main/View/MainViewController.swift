//
//  MainViewController.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/8/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import UIKit

protocol MainViewProtocol: class {
    
}

class MainViewController: UITabBarController {

    var presenter: MainPresenterProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        guard let navMessageViewController = self.viewControllers?.first as? UINavigationController, let messageViewController = navMessageViewController.viewControllers.first as? MessageViewController else { return }
        let messagePresenter = MessagePresenter()
        messageViewController.presenter = messagePresenter
        messagePresenter.view = messageViewController
        
        guard let navSettingViewController = self.viewControllers?.last as? UINavigationController, let settingViewController = navSettingViewController.viewControllers.first as? SettingViewController else { return }
        let settingPresenter = SettingPresenter()
        settingPresenter.view = settingViewController
        settingViewController.presenter = settingPresenter
    }

}

extension MainViewController: MainViewProtocol {
    
}
