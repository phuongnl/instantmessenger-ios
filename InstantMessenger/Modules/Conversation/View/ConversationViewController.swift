//
//  ConversationViewController.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/23/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import UIKit
import MessageKit
import InputBarAccessoryView

protocol ConversationViewProtocol: class {
    var presenter: ConversationPresenterProtocol! { get set }
}

class ConversationViewController: MessagesViewController, ConversationViewProtocol {

    var presenter: ConversationPresenterProtocol!
    var sender: MKSender!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = L10n.conversation
    }

}

extension ConversationViewController: MessagesDataSource {
    
    func currentSender() -> SenderType {
        return self.sender
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return MKMessage(text: "123", user: MKSender(senderId: "1", displayName: "123"), messageId: "123", date: Date())
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return 1
    }
    
}

extension ConversationViewController: MessagesLayoutDelegate {
    
    
}

extension ConversationViewController: MessagesDisplayDelegate {
    
}
