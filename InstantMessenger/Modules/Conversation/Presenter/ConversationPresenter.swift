//
//  ConversationViewModel.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/23/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import Foundation

protocol ConversationPresenterProtocol: class {
    var view: ConversationViewProtocol! { get set }
}

class ConversationPresenter: ConversationPresenterProtocol {
    
    var view: ConversationViewProtocol!
    let messageRepository = MessageRepository()
    
}
