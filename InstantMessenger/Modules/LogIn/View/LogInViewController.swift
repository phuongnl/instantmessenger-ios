//
//  LogInViewController.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/8/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol LogInViewProtocol: class {
    func showRegisterView()
    func showMainView()
    func showAlert(message: String?)
}

class LogInViewController: UIViewController {

    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    
    var presenter: LogInPresenterProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = L10n.logIn
    }

    @IBAction func actionLogIn(_ sender: UIButton) {
        guard let email = self.textFieldEmail.text, let password = self.textFieldPassword.text else { return }
        self.presenter.handleLogIn(email: email, password: password)
    }
    
    @IBAction func actionRegister(_ sender: UIButton) {
        guard let email = self.textFieldEmail.text, let password = self.textFieldPassword.text else { return }
        self.presenter.handleRegister(email: email, password: password)
    }
    
}

extension LogInViewController: LogInViewProtocol {
    
    func showRegisterView() {
        let registerViewController: RegisterViewController = UIStoryboard(.register).instantiateViewController()
        let registerPresenter = RegisterPresenter()
        registerPresenter.view = registerViewController
        registerViewController.presenter = registerPresenter
        self.navigationController?.pushViewController(registerViewController, animated: true)
    }
    
    func showMainView() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let mainViewController: MainViewController = UIStoryboard(.main).instantiateViewController(withIdentifier: "MainViewController")
        let mainPresenter = MainPresenter()
        mainPresenter.view = mainViewController
        mainViewController.presenter = mainPresenter
        appDelegate.window?.rootViewController = mainViewController
    }
    
    func showAlert(message: String?) {
        SVProgressHUD.showInfo(withStatus: message)
    }
    
}
