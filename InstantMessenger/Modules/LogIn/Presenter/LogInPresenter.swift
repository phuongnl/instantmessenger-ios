//
// LogInPresenter.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/8/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import Foundation

protocol LogInPresenterProtocol: class {
    func handleLogIn(email: String, password: String)
    func handleRegister(email: String, password: String)
}

class LogInPresenter: LogInPresenterProtocol {

    var view: LogInViewProtocol!
    let userRepository =  UserRepository()
    
    func handleLogIn(email: String, password: String) {
        if email.isEmpty || password.isEmpty {
            self.view.showAlert(message: "Passwrod or Email is blank")
        } else if email == "admin" && password == "123456" {
            self.view.showMainView()
        } else {
            self.view.showAlert(message: "Email = admin \nPassword = 123456")
        }
        self.userRepository.login()
    }
    
    func handleRegister(email: String, password: String) {
        self.view.showRegisterView()
    }
    
}
