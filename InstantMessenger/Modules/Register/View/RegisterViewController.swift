//
//  RegisterViewController.swift
//  InstantMessenger
//
//  Created by Lucas Lee on 7/8/19.
//  Copyright © 2019 Lucas Lee. All rights reserved.
//

import UIKit

protocol RegisterViewProtocol: class {
    
}

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    
    var presenter: RegisterPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = L10n.register
    }
    
    @IBAction func actionRegister(_ sender: UIButton) {
        
    }
    
}

extension RegisterViewController: RegisterViewProtocol {
    
}
