# InstantMessenger-iOS

Instant messaging and information sharing system - iOS Application using Core Business Service and MVP Architect

## Install Carthage

`carthage upate --platform ios`

## Install Pods

`pod install`
